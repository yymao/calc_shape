# calc_shape

A Python wrapper of the `calc_shape` function in [Peter Behroozi](http://www.peterbehroozi.com/)'s [Rockstar](https://bitbucket.org/gfcstanford/rockstar) code.

## Installation

    pip install https://bitbucket.org/yymao/calc_shape/get/master.zip

## Usage

    from calc_shape import calc_shape
    b_to_a, c_to_a, eig_A = calc_shape(halo_rvir, halo_pos, particle_pos, force_res)


