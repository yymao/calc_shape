"""
Project website: https://bitbucket.org/yymao/calc_shape
Author: Yao-Yuan Mao (yymao)
"""
import os
from subprocess import check_call
from setuptools import setup

_lib_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'calc_shape')
_make_pre = 'gcc -D_BSD_SOURCE -D_POSIX_SOURCE -D_POSIX_C_SOURCE=200809L -D_SVID_SOURCE -D_DARWIN_C_SOURCE -Wall -fno-math-errno -fPIC -shared'.split()
_make_post = '-lm -O3 -std=c99'.split()
_cwd = os.getcwd()
os.chdir(_lib_path)
check_call(_make_pre + ['calc_shape.c', 'jacobi.c', '-o', 'calc_shape.so'] + _make_post)
os.chdir(_cwd)


setup(
    name='calc_shape',
    version='0.1.1',
    description='A Python wrapper of the `calc_shape` function in Peter Behroozi\'s Rockstar code.',
    url='https://bitbucket.org/yymao/calc_shape',
    author='Yao-Yuan Mao, Peter Behroozi',
    author_email='Yao-Yuan Mao <yymao.astro@gmail.com>',
    license='GPLv3',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering :: Astronomy',
    ],
    packages=['calc_shape'],
    package_data={
        'calc_shape': ['calc_shape.so'],
    },
    install_requires = ['numpy'],
)
