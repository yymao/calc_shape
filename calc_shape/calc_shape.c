#include <math.h>
#include <string.h>
#include <inttypes.h>
#include "jacobi.h"


void calc_shape(double * halo_A, double halo_r, double * halo_pos, double * po,
    int64_t total_p, double FORCE_RES, int64_t iter, int64_t WEIGHTED_SHAPES){

  int64_t i, j, k, l, j3, a, b, c;
  double b_to_a, c_to_a, min_r = FORCE_RES*FORCE_RES;
  double mass_t[3][3], orth[3][3], eig[3]={0}, r=0, dr, dr2, weight=0;

  if (!(halo_r>0)) return;
  min_r *= 1e6 / (halo_r*halo_r);
  if (total_p < 3 || !(halo_r>0)) return;
  if (total_p < iter) iter = total_p;

  for (i=0; i<3; i++) {
    memset(orth[i], 0, sizeof(double)*3);
    orth[i][i] = 1;
    eig[i] = (halo_r*halo_r)*1e-6;
  }
  for (i=0; i<iter; i++) {
    for (k=0; k<3; k++) {for (j=0; j<3; j++) mass_t[k][j]=0;}
    weight=0;
    for (j=0; j<total_p; j++) {
      r=0;
      j3 = j*3;
      for (k=0; k<3; k++) {
        for (dr=0,l=0; l<3; l++) {
          dr += orth[k][l]*(po[j3+l]-halo_pos[l]);
        }
        r += dr*dr/eig[k];
            }
            if (r < min_r) r = min_r;
            if (!(r>0 && r<=1)) continue;
            double tw = (WEIGHTED_SHAPES) ? 1.0/r : 1.0;
            weight+=tw;
            for (k=0; k<3; k++) {
        dr = po[j3+k]-halo_pos[k];
        mass_t[k][k] += dr*dr*tw;
        for (l=0; l<k; l++) {
          dr2 = po[j3+l]-halo_pos[l];
          mass_t[k][l] += dr2*dr*tw;
          mass_t[l][k] = mass_t[k][l];
        }
      }
    }

    if (!weight) return;
    for (k=0; k<3; k++) for (l=0; l<3; l++) mass_t[k][l] /= (double)weight;
    jacobi_decompose(mass_t, eig, orth);
    a = 0; b = 1; c = 2;
    if (eig[1]>eig[0]) { b=0; a=1; }
    if (eig[2]>eig[b]) { c=b; b=2; }
    if (eig[b]>eig[a]) { int64_t t=a; a=b; b=t; }
    if (!eig[a] || !eig[b] || !eig[c]) return;
    b_to_a = sqrt(eig[b]/eig[a]);
    c_to_a = sqrt(eig[c]/eig[a]);
    if ((fabs(b_to_a-halo_A[3]) < 0.01*halo_A[3]) &&
        (fabs(c_to_a-halo_A[4]) < 0.01*halo_A[4])) return;
    halo_A[3] = (b_to_a > 0) ? b_to_a : 0;
    halo_A[4] = (c_to_a > 0) ? c_to_a : 0;
    r = sqrt(eig[a]);
    for (k=0; k<3; k++) {
      halo_A[k] = 1e3*r*orth[a][k];
      eig[k] *= (halo_r*halo_r*1e-6)/(r*r);
    }
  }
}
