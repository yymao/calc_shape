__all__ = ['calc_shape']
import os
import numpy as np
import numpy.ctypeslib as C

# define types
_double_ctype = C.ndpointer(np.float64, ndim=1, flags='C')

# load the c library
# void calc_shape(double * halo_A, double halo_r, double * halo_pos, double * po, int64_t total_p, double FORCE_RES)
here = os.path.abspath(os.path.dirname(__file__))
_C_LIB = C.ctypes.cdll[os.path.join(here, 'calc_shape.so')]
_C_LIB.calc_shape.restype = None
_C_LIB.calc_shape.argtypes = [
    _double_ctype,
    C.ctypes.c_double,
    _double_ctype,
    _double_ctype,
    C.ctypes.c_int,
    C.ctypes.c_double,
    C.ctypes.c_int,
    C.ctypes.c_int,
]

def calc_shape(halo_rvir, halo_pos, particle_pos, force_res, iteration=10, weighted=True):
    """
    calculate halo shape (b/a, c/a and the fist eigen vector)

    Parameters
    ----------
    halo_rvir : float
        radius of the halo (should in the the same unit as halo_pos, particle_pos, and force_res)
    halo_pos : numpy.ndarray of float, with shape (3,)
        spatial position of the halo center [x, y, z]
    particle_pos : numpy.ndarray of float, with shape (N, 3)
        positions of particles [[x1, y1, z1], [x2, y2, z2], ...]
    force_res : float
        force resolution (softening length)
    iteration : int, optional
        number of iteration (default: 10)
    weighted : bool, optional
        whether or not to weighted each particle by 1/r (default: True)

    Returns
    -------
    b_to_a : float
        b/a value
    c_to_a : float
        c/a value
    eig_A : numpy.ndarray of float, with shape (3,)
        the eigen vector of the largest eigenvalue (a)
    """
    output = np.zeros((5,), np.float64)
    halo_rvir = np.float64(halo_rvir*1.0e3)
    halo_pos = np.asarray(halo_pos, np.float64).ravel()
    particle_pos = np.asarray(particle_pos, np.float64).ravel()
    force_res = np.float64(force_res)
    total_p = np.int64(len(particle_pos) // 3)
    iteration = np.int64(iteration)
    weighted = np.int64(weighted)
    _C_LIB.calc_shape(output, halo_rvir, halo_pos, particle_pos, total_p, force_res, iteration, weighted)
    return output[3], output[4], output[:3]
